package utils;

import dictionary.IDictionary;

import java.util.*;

public class Utils {

//    private static Comparator<IDictionary> dataComparator = Comparator.comparing(IDictionary::getLemma);

    public static void setLexicograficalOrder(List<IDictionary> results) {
        results.sort(new DictionaryComparator());
    }

    private static void reverseLemma(List<IDictionary> results) {
        for (IDictionary data: results) {
            StringBuilder lemma = new StringBuilder(data.getLemma());
            data.setLemma(lemma.reverse().toString());
        }
    }

    public static void setInverseOrder(List<IDictionary> results) {
        reverseLemma(results);
        results.sort(new DictionaryComparator());
        reverseLemma(results);
    }
}
