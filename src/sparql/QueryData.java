package sparql;

import enums.DictionaryType;

public class QueryData {

    public static String getQueryForOne(DictionaryType type, String word) {
        String query = "PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\n" +
                "PREFIX dct: <http://purl.org/dc/terms/>\n" +
                "PREFIX wikibase: <http://wikiba.se/ontology#>\n" +
                "PREFIX wd: <http://www.wikidata.org/entity/>\n" +
                "PREFIX wdt: <http://www.wikidata.org/prop/direct/>\n" +
                "PREFIX bd: <http://www.bigdata.com/rdf#>\n" +
                "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n";

        switch (type) {
//            case Test: {
//                query = "select ?x ?y where {\n" +
//                        "  values ?x { 1 2 3 4 }\n" +
//                        "  {\n" +
//                        "    select ?y where {\n" +
//                        "      values ?y { 5 6 7 8 }\n" +
//                        "    }\n" +
//                        "    limit 2\n" +
//                        "  }\n" +
//                        "}\n" +
//                        "limit 5";
//                break;
//            }
            case Grammatical: {
                query += "SELECT ?myPart ?gender ?form ?wordLabel (GROUP_CONCAT(DISTINCT ?grammaticalFeature; SEPARATOR=', ') AS ?lem)\n" +
                        "WHERE {\n" +
                        "  ?l A ontolex:LexicalEntry ;\n" +
                        "    dct:language wd:Q7737 ;\n" +
                        "    wikibase:lexicalCategory ?myPart ;\n" +
                        "    wikibase:lemma ?lemma ;\n" +
                        "    ontolex:lexicalForm ?form .\n" +
                        "    VALUES ?myPart { wd:Q1084 wd:Q34698 wd:Q24905 wd:Q380057 }\n" +
                        "  ?form ontolex:representation ?word ;\n" +
                        "    wikibase:grammaticalFeature ?grammaticalFeature .\n" +
                        "  OPTIONAL { ?l wdt:P5185 ?gender }\n" +
                        String.format("  FILTER (STR(?lemma) = \"%s\") .\n", word) +
                        "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en\". }\n" +
                        "} GROUP BY ?myPart ?gender ?form ?wordLabel ?lem";
                break;
            }
            case Explanatory: {
                query += "SELECT ?senseLabel\n" +
                        "WHERE {\n" +
                        "  ?l A ontolex:LexicalEntry ;\n" +
                        "    dct:language wd:Q7737 ;\n" +
                        "    wikibase:lexicalCategory ?myPart;\n" +
                        "    wikibase:lemma ?lemma;\n" +
                        "    ontolex:sense/skos:definition ?sense.\n" +
                        "  VALUES ?myPart { wd:Q1084 wd:Q24905 wd:Q34698 wd:Q380057 }\n" +
                        "  FILTER (LANG(?sense)='ru').\n" +
                        String.format("  FILTER (STR(?lemma) = \"%s\") .\n", word) +
                        "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en\". }\n" +
                        "}";
                break;
            }
            case Translation: {
                query += "SELECT ?sense ?definitionLabel ?meaning ?transLemma ?languageLabel\n" +
                        "WHERE {\n" +
                        "  ?l a ontolex:LexicalEntry ;\n" +
                        "       dct:language wd:Q7737 ;\n" +
                        "     wikibase:lexicalCategory ?myPart ;\n" +
                        "     wikibase:lemma ?lemma ;\n" +
                        "     ontolex:sense ?sense ;\n" +
                        "     ontolex:sense/skos:definition ?definition .\n" +
                        "  values ?myPart { wd:Q1084 wd:Q24905 wd:Q34698 wd:Q380057 }\n" +
                        "  ?sense wdt:P5972 ?meaning .\n" +
                        "  ?translex ontolex:sense ?meaning .\n" +
                        "  ?translex wikibase:lemma ?transLemma .\n" +
                        "  ?translex dct:language ?language .\n" +
                        "  FILTER (LANG(?definition)= 'ru') .\n" +
                        String.format("  FILTER (STR(?lemma) = \"%s\") .\n", word) +
                        "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en\"}\n" +
                        "}";
                break;
            }
        }
        return query;
    }

//    public static String getQueryForAll(DictionaryType type, String word) {
//        String query = "";
//        switch (type) {
////            case Test: {
////                query = "select ?x ?y where {\n" +
////                        "  values ?x { 1 2 3 4 }\n" +
////                        "  {\n" +
////                        "    select ?y where {\n" +
////                        "      values ?y { 5 6 7 8 }\n" +
////                        "    }\n" +
////                        "    limit 2\n" +
////                        "  }\n" +
////                        "}\n" +
////                        "limit 5";
////                break;
////            }
//            case Grammatical: {
//                query = "PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\n" +
//                        "PREFIX dct: <http://purl.org/dc/terms/>\n" +
//                        "PREFIX wikibase: <http://wikiba.se/ontology#>\n" +
//                        "PREFIX wd: <http://www.wikidata.org/entity/>\n" +
//                        "PREFIX wdt: <http://www.wikidata.org/prop/direct/>\n" +
//                        "PREFIX bd: <http://www.bigdata.com/rdf#>\n" +
//                        "SELECT DISTINCT ?lemmaLabel ?myPart ?genderLabel \n" + //?myPart ?genderLabel ?wordLabel (GROUP_CONCAT(DISTINCT ?grammaticalFeature; SEPARATOR=', ') AS ?lem)\n" +
//                        "WHERE {\n" +
//                        "  ?l a ontolex:LexicalEntry;\n" +
//                        "    dct:language wd:Q7737;\n" +
//                        "    wikibase:lexicalCategory ?myPart;\n" +
//                        "    wikibase:lemma ?lemma.\n" + //;
////                        "    ontolex:lexicalForm ?form.\n" +
//                        "    values ?myPart { wd:Q1084 wd:Q34698 wd:Q24905 wd:Q380057 } \n" + // #wd:Q1084
////                        "  ?form ontolex:representation ?word;\n" +
////                        "    wikibase:grammaticalFeature ?grammaticalFeature.\n" +
//                        "  optional { ?l wdt:P5185 ?gender }\n" +
////                        getDictionaryWithOneWord(word) +
//                        "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE]\". }\n" +
//                        "} LIMIT 10000"; //  GROUP BY ?lemmaLabel ?myPart ?genderLabel ?wordLabel ?lem
//                break;
//            }
//            case Explanatory: {
//                query =
//                        "PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\n" +
//                        "PREFIX dct: <http://purl.org/dc/terms/>\n" +
//                        "PREFIX wikibase: <http://wikiba.se/ontology#>\n" +
//                        "PREFIX wd: <http://www.wikidata.org/entity/>\n" +
//                        "PREFIX wdt: <http://www.wikidata.org/prop/direct/>\n" +
//                        "PREFIX bd: <http://www.bigdata.com/rdf#>\n" +
//                        "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
//                        "SELECT DISTINCT ?lemmaLabel WHERE {\n" + //?senseLabel
//                        "  ?l a ontolex:LexicalEntry;\n" +
//                        "    dct:language wd:Q7737 ;\n" +
//                        "    wikibase:lexicalCategory ?myPart;\n" +
//                        "    wikibase:lemma ?lemma;\n" + //;
//                        "    ontolex:sense/skos:definition ?sense.\n" +
//                        "  values ?myPart { wd:Q1084 wd:Q24905 wd:Q34698 wd:Q380057 }\n" +
//                        "  FILTER (LANG(?sense)='ru').\n" +
////                        getDictionaryWithOneWord(word) +
//                        "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en\". }\n" +
//                        "}";
//                break;
//            }
//        }
//        return query;
//    }


    public static String getQueryForAll(DictionaryType type) {
        String query = "PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\n" +
                "PREFIX dct: <http://purl.org/dc/terms/>\n" +
                "PREFIX wikibase: <http://wikiba.se/ontology#>\n" +
                "PREFIX wd: <http://www.wikidata.org/entity/>\n" +
                "PREFIX wdt: <http://www.wikidata.org/prop/direct/>\n" +
                "PREFIX bd: <http://www.bigdata.com/rdf#>\n" +
                "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n";

        switch (type) {
            case Grammatical: {
                query += "SELECT DISTINCT ?lemma\n" +
                        "WHERE {\n" +
                        "  ?l A ontolex:LexicalEntry ;\n" +
                        "    dct:language wd:Q7737 ;\n" +
                        "    wikibase:lexicalCategory ?myPart ;\n" +
                        "    wikibase:lemma ?lemma .\n" +
                        "    VALUES ?myPart { wd:Q1084 wd:Q34698 wd:Q24905 wd:Q380057 }\n" +
                        "}\n";
                break;
            }
            case Explanatory: {
                query += "SELECT DISTINCT ?lemma\n" +
                        "WHERE {\n" +
                        "  ?l A ontolex:LexicalEntry ;\n" +
                        "    dct:language wd:Q7737 ;\n" +
                        "    wikibase:lexicalCategory ?myPart ;\n" +
                        "    wikibase:lemma ?lemma ;\n" +
                        "    ontolex:sense/skos:definition ?sense .\n" +
                        "  VALUES ?myPart { wd:Q1084  wd:Q24905 wd:Q34698 wd:Q380057 }\n" +
                        "  FILTER (LANG(?sense)= 'ru') .\n" +
                        "}";
                break;
            }
            case Translation: {
                query += "SELECT DISTINCT ?lemma\n" +
                        "WHERE {\n" +
                        "  ?l A ontolex:LexicalEntry ; \n" +
                        "       dct:language wd:Q7737 ;\n" +
                        "     wikibase:lexicalCategory ?myPart ;\n" +
                        "     wikibase:lemma ?lemma ;\n" +
                        "     ontolex:sense ?sense ;\n" +
                        "     ontolex:sense/skos:definition ?definition .\n" +
                        "  VALUES ?myPart { wd:Q1084  wd:Q24905 wd:Q34698 wd:Q380057 }\n" +
                        "  ?sense wdt:P5972 ?meaning .\n" +
                        "  ?translex ontolex:sense ?meaning .\n" +
                        "  ?translex wikibase:lemma ?transLemma .\n" +
                        "  ?translex dct:language ?language .\n" +
                        "  FILTER (LANG(?definition)= 'ru') .\n" +
                        "}";
                break;
            }
        }
        return query;
//        return "PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\n" +
//                "PREFIX dct: <http://purl.org/dc/terms/>\n" +
//                "PREFIX wikibase: <http://wikiba.se/ontology#>\n" +
//                "PREFIX wd: <http://www.wikidata.org/entity/>\n" +
//                "PREFIX wdt: <http://www.wikidata.org/prop/direct/>\n" +
//                "PREFIX bd: <http://www.bigdata.com/rdf#>\n" +
//                (type == DictionaryType.Explanatory ? "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" : "") +
//                "SELECT DISTINCT ?lemmaLabel \n" +
//                "WHERE {\n" +
//                "  ?l a ontolex:LexicalEntry;\n" +
//                "    dct:language wd:Q7737;\n" +
//                "    wikibase:lexicalCategory ?myPart;\n" +
//                "    wikibase:lemma ?lemma" +
//                (type != DictionaryType.Explanatory ? ".\n" :
//                        ";\n" +
//                        "    ontolex:sense/skos:definition ?sense.\n" +
//                        "  FILTER (LANG(?sense)='ru').\n") +
//                "    values ?myPart { wd:Q1084 wd:Q34698 wd:Q24905 wd:Q380057 } \n" +
//                "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],en\". }\n" +
//                (type == DictionaryType.Grammatical ? "}\n limit 10000" : "}");
    }
}
