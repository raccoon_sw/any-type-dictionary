package enums;

public enum DictionaryType {
//    Test,
    Grammatical, //грамматический
    Explanatory, //толковый
    Translation
}
