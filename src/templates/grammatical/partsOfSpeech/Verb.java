package templates.grammatical.partsOfSpeech;

import dictionary.grammatical.WordForms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Verb {

    private static List<String> someFeatures = Arrays.asList("первое лицо", "второе лицо", "третье лицо");

    private static List<String> setCommunionForVerbs(List<WordForms> caseWordForms) {
        List<WordForms> wordsCommunion = caseWordForms.stream().filter(word ->
                word.getLemmas().contains("причастие") && word.getLemmas().contains("действительный залог"))
                .collect(Collectors.toList());
        String wordsCommunionPresent = wordsCommunion.stream()
                .filter(word -> word.getLemmas().contains("настоящее время"))
                .map(WordForms::getWord).collect(Collectors.joining(",\n"));
        String wordsCommunionPast = wordsCommunion.stream()
                .filter(word -> word.getLemmas().contains("прошедшее время"))
                .map(WordForms::getWord).collect(Collectors.joining(",\n"));
        return Arrays.asList("причастие\n(действительный залог)", wordsCommunionPresent, wordsCommunionPast);
    }

    private static List<String> setParticipleForVerbs(List<WordForms> caseWordForms) {
        List<WordForms> wordsParticiple = caseWordForms.stream()
                .filter(word -> word.getLemmas().contains("деепричастие"))
                .collect(Collectors.toList());
        String wordsParticiplePresent = wordsParticiple.stream()
                .filter(word -> word.getLemmas().contains("настоящее время"))
                .map(WordForms::getWord).collect(Collectors.joining(",\n"));
        String wordsParticiplePast = wordsParticiple.stream()
                .filter(word -> word.getLemmas().contains("прошедшее время"))
                .map(WordForms::getWord).collect(Collectors.joining(",\n"));
        return Arrays.asList("деепричастие", wordsParticiplePresent, wordsParticiplePast);
    }

    private static List<String> setSingularsOrPluralsForVerbs(List<WordForms> caseWordForms) {
        List<WordForms> wordsImperative = caseWordForms.stream().filter(word ->
                word.getLemmas().contains("повелительное наклонение")).collect(Collectors.toList());
        List<WordForms> wordsIndicative = caseWordForms.stream().filter(word ->
                word.getLemmas().contains("изъявительное наклонение")).collect(Collectors.toList());
        List<WordForms> wordsIndicativePresent = wordsIndicative.stream().filter(word ->
                word.getLemmas().contains("настоящее время")).collect(Collectors.toList());
        System.out.println(caseWordForms.stream().filter(word ->
                word.getLemmas().contains("повелительное наклонение")).collect(Collectors.toList()).size());
        List<WordForms> wordsIndicativePast = wordsIndicative.stream().filter(word ->
                word.getLemmas().contains("прошедшее время")).collect(Collectors.toList());

        return Arrays.asList(
                Noun.setSingularsOrPluralsForNouns(true, wordsIndicativePresent),
                Noun.setSingularsOrPluralsForNouns(false, wordsIndicativePresent),
                Noun.setSingularsOrPluralsForNouns(true, wordsIndicativePast),
                Noun.setSingularsOrPluralsForNouns(false, wordsIndicativePast),
                Noun.setSingularsOrPluralsForNouns(true, wordsImperative),
                Noun.setSingularsOrPluralsForNouns(false, wordsImperative));
    }

    private static List<List<String>> getWordsForVerbs(List<WordForms> wordForms) {
        List<List<String>> features = new ArrayList<>();
        for (String featureName : someFeatures) {
            List<WordForms> featureWordFormsWithGender = wordForms.stream()
                    .filter(word -> word.getLemmas().contains(featureName) && (word.getLemmas().contains("мужской род")
                            || word.getLemmas().contains("женский род") || word.getLemmas().contains("средний род")))
                    .collect(Collectors.toList());
            List<WordForms> featureWordForms = wordForms.stream()
                    .filter(word -> word.getLemmas().contains(featureName) && (!word.getLemmas().contains("мужской род")
                            || !word.getLemmas().contains("женский род") || !word.getLemmas().contains("средний род")))
                    .collect(Collectors.toList());
            List<String> tmp = new ArrayList<>();
            if (featureWordForms.size() > 0) {
                tmp.add(featureName);
                tmp.addAll(setSingularsOrPluralsForVerbs(featureWordForms));
            }
            if (featureWordFormsWithGender.size() > 0) {
                tmp.addAll(setSingularsOrPluralsForVerbs(featureWordFormsWithGender));
            }
            if (tmp.size() > 0)
                features.add(tmp);
        }
        return features;
    }

    public static String setVerbTable(List<WordForms> wordForms) {
        StringBuilder htmlPage = new StringBuilder("<table border=\"1\" cellspacing=\"0\"\n" +
                "  cellpadding=\"10\">\n" +
                "  <thead>\n" +
                "    <tr>\n" +
                "      <th bgcolor=\"#EEF9FF\" rowspan=\"3\"></th>\n" +
                "      <th bgcolor=\"#EEF9FF\" colspan=\"4\">изъявительное наклонение</th>\n" +
                "      <th bgcolor=\"#EEF9FF\" colspan=\"2\">повелительное наклонение</th>\n" +
                "    </tr>\n" +
                "    <tr bgcolor=\"#EEF9FF\">\n" +
                "      <th colspan=\"2\">настоящее время</th>\n" +
                "      <th colspan=\"2\">прошедшее время</th>\n" +
                "      <th rowspan=\"2\">единственное число</th>\n" +
                "      <th rowspan=\"2\">множественное число</th>\n" +
                "    </tr>\n" +
                "    <tr bgcolor=\"#EEF9FF\">\n" +
                "      <th>единственное число</th>\n" +
                "      <th>множественное число</th>\n" +
                "      <th>единственное число</th>\n" +
                "      <th>множественное число</th>\n" +
                "    </tr>\n" +
                "  </thead>\n" +
                "  <tbody>\n");
        String table;
        for (List<String> wordsData : getWordsForVerbs(wordForms)) {
            table = String.format("<tr>\n" +
                            "  <td bgcolor=\"#EEF9FF\">%s</td>\n" +
                            "  <td>%s</td>\n" +
                            "  <td>%s</td>\n" +
                            "  <td>%s</td>\n" +
                            "  <td>%s</td>\n" +
                            "  <td>%s</td>\n" +
                            "  <td>%s</td>\n" +
                            "</tr>\n",  wordsData.get(0), wordsData.get(1), wordsData.get(2),
                    wordsData.get(3), wordsData.get(4), wordsData.get(5), wordsData.get(6));
            htmlPage.append(table);
        }
        List<List<String>> tmp = new ArrayList<>();
        tmp.add(setParticipleForVerbs(wordForms));
        tmp.add(setCommunionForVerbs(wordForms));
        for (List<String> wordsData : tmp) {
            table = String.format("<tr>\n" +
                            "  <td bgcolor=\"#EEF9FF\">%s</td>\n" +
                            "  <td colspan=\"2\">%s</td>\n" +
                            "  <td colspan=\"2\">%s</td>\n" +
                    "  <td></td>\n" +
                    "  <td></td>\n" +
                            "</tr>\n", wordsData.get(0), wordsData.get(1), wordsData.get(2));
            htmlPage.append(table);
        }
        htmlPage.append("  </tbody></table>\n");
        return htmlPage.toString();
    }

}
