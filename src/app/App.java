package app;

import configuration.PathConfig;
import frontend.controllers.MainViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.awt.*;
import java.net.URL;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        URL url = getClass().getResource(PathConfig.getMainViewPath());
        FXMLLoader root = new FXMLLoader(url);
        MainViewController controller = new MainViewController(primaryStage);
        root.setController(controller);
        AnchorPane anchorPane = root.load();
        primaryStage.setTitle("Словарь любого типа");
        primaryStage.getIcons().add(new Image(PathConfig.getIcoMainPath()));
        primaryStage.setMinHeight(680);
        primaryStage.setMinWidth(1127);
        primaryStage.setMaxHeight(Toolkit.getDefaultToolkit().getScreenSize().height);
        primaryStage.setMaxWidth(Toolkit.getDefaultToolkit().getScreenSize().width);
        primaryStage.setScene(new Scene(anchorPane));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        //save current session
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
