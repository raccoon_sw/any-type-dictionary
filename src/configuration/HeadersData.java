package configuration;

import enums.DictionaryType;

import java.util.Arrays;
import java.util.List;

public class HeadersData {

    private List<String> types = Arrays.asList(
//            "Тестовый",
            "Грамматический",
            "Толковый"
    );

    public static DictionaryType getDictionaryType(String typeName) {
        DictionaryType dictionaryType = null;
        switch (typeName) {
//            case "Тестовый": {
//                dictionaryType = DictionaryType.Test;
//                break;
//            }
            case "Грамматический": {
                dictionaryType = DictionaryType.Grammatical;
                break;
            }
            case "Толковый": {
                dictionaryType = DictionaryType.Explanatory;
                break;
            }
        }
        return dictionaryType;
    }

    public List<String> getTypesName() {
        return types;
    }
}
